:: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
:: @ Author: Satya Bandla                               @
:: @ Date: Jan 2024                                     @
:: @ Purpose:                                           @
:: @ This batch script helps in doing the below process @
:: @    > Backup destination path                       @
:: @    > Copy source data into destination location    @
:: @    > Restart pool application                      @
:: @                                                    @
:: @ Takes the below user inputs                        @
:: @    > Environment - DEV/UAT/CRPRJ/SIT/TR_PR         @
:: @    > Pool App - WebApp/Rater/WebService            @
:: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo off
cls
echo.
set SERVER-AGII_APDEVTW01=agii-apdevtw01
set SERVER-AGII_APSITW01=agii-apsitw01
set SERVER-AGII_APTRNQW01=agii-aptrnqw01

:: Performance Server for WebApp and Rater
set SERVER-AGII_APPRFTW01=agii-apprftw01
set SERVER-AGII_APPRFTW02=agii-apprftw02

:: Performance Server for WebService
set SERVER-AGII-R3APPRFW01=agii-r3apprfw01
set SERVER-AGII-R3APPRFW02=agii-r3apprfw02

echo Start of Process
echo.

:: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
:: @ User Input Validation       @
:: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo              User Inputs
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if [%1]==[] goto argnotfound
if [%2]==[] goto argnotfound
if [%3]==[] goto argnotfound
if [%4]==[] goto argnotfound
set ENVIRONMENT=%1
set APPNAME=%2
set UNAME=%3
set UPWD=%4
echo Environment: %ENVIRONMENT%
echo Pool Application Name: %APPNAME%
echo User Name: %UNAME%

:: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
:: @ Generate Backup file name with date and timestamp @
:: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
:: Check WMIC is available
WMIC.EXE Alias /? >NUL 2>&1 || GOTO s_error

:: Use WMIC to retrieve date and time
FOR /F "skip=1 tokens=1-6" %%G IN ('WMIC Path Win32_LocalTime Get Day^,Hour^,Minute^,Month^,Second^,Year /Format:table') DO (
   IF "%%~L"=="" goto s_done
      set _yyyy=%%L
      set _mm=00%%J
      set _dd=00%%G
      set _hour=00%%H
      set _minute=00%%I
)
:s_done

:: Pad digits with leading zeros
      set _mm=%_mm:~-2%
      set _dd=%_dd:~-2%
      set _hour=%_hour:~-2%
      set _minute=%_minute:~-2%

:: Display the date/time in ISO 8601 format:
set _isodate=%_yyyy%%_mm%%_dd%-%_hour%%_minute%
:: echo Current Date: %_isodate%

set LOGFILE=ColonyRater-%ENVIRONMENT%-%APPNAME%-%_isodate%.log
set SOURCEPATH=\\argous.com\chi_shares\SHARED\EdgeAdmins\ColonyRater\%ENVIRONMENT%\%APPNAME%\

echo.
echo. @@@ Logs are captured in %LOGFILE% @@@
echo. 

echo User Input: > %LOGFILE%
echo =========== >> %LOGFILE%
echo Environment: %ENVIRONMENT% >> %LOGFILE%
echo Pool Application Name: %APPNAME% >> %LOGFILE%
echo.  >> %LOGFILE%

if "%APPNAME%"=="WebApp" (
	if "%ENVIRONMENT%"=="DEV" (
		set SERVERNAME=%SERVER-AGII_APDEVTW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APDEVTW01%\D$\Inetpub\Int\CR.App.INT.Maintenance
		set POOLAPPNAME=CR.App.INT.Maintenance
	)
	if "%ENVIRONMENT%"=="UAT" (
		set SERVERNAME=%SERVER-AGII_APDEVTW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APDEVTW01%\D$\Inetpub\Int\UAT\CR.App.UAT.Maintenance
		set POOLAPPNAME=CR.App.UAT.Maintenance
	)
	if "%ENVIRONMENT%"=="CRPRJ" (
		set SERVERNAME=%SERVER-AGII_APDEVTW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APDEVTW01%\D$\Inetpub\Int\CR.App.INT.AcordIssuance
		set POOLAPPNAME=CR.App.Int.AcordIssuance
	)
	if "%ENVIRONMENT%"=="TR_PR" (
		set SERVERNAME=%SERVER-AGII_APDEVTW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APDEVTW01%\D$\Inetpub\Int\CR.App.INT.Maintenance-TR_Filing
		set POOLAPPNAME=CR.App.INT.Maintenance-TR_Filing
	)
	if "%ENVIRONMENT%"=="SIT" (
		set SERVERNAME=%SERVER-AGII_APSITW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APSITW01%\D$\Inetpub\WebSite
		set POOLAPPNAME=ColonyRater
	)
	if "%ENVIRONMENT%"=="TRN" (
		set SERVERNAME=%SERVER-AGII_APTRNQW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APTRNQW01%\D$\Inetpub\WebSite
		set POOLAPPNAME=ColonyRater
	)	
	if "%ENVIRONMENT%"=="PERF01" (
		set SERVERNAME=%SERVER-AGII_APPRFTW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APPRFTW01%\D$\Inetpub\WebSite
		set POOLAPPNAME=ColonyRater.Perf
	)	
	if "%ENVIRONMENT%"=="PERF02" (
		set SERVERNAME=%SERVER-AGII_APPRFTW02%
		set DESTINATIONPATH=\\%SERVER-AGII_APPRFTW02%\D$\Inetpub\WebSite
		set POOLAPPNAME=ColonyRater.Perf
	)	
)
if "%APPNAME%"=="Rater" (
	if "%ENVIRONMENT%"=="DEV" (
		set SERVERNAME=%SERVER-AGII_APDEVTW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APDEVTW01%\D$\Inetpub\Int\CR.Maintenance.INT.Maintenance
		set POOLAPPNAME=CR.App.Int.Maintenance
	)
	if "%ENVIRONMENT%"=="UAT" (
		set SERVERNAME=%SERVER-AGII_APDEVTW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APDEVTW01%\D$\Inetpub\Int\UAT\CR.Maintenance.UAT.Maintenance
		set POOLAPPNAME=CR.App.UAT.Maintenance
	)
	if "%ENVIRONMENT%"=="CRPRJ" (
		set SERVERNAME=%SERVER-AGII_APDEVTW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APDEVTW01%\D$\Inetpub\Int\UAT\CR.Maintenance.UAT.Maintenance
		set POOLAPPNAME=CR.App.Int.AcordIssuance
	)
	if "%ENVIRONMENT%"=="TR_PR" (
		set SERVERNAME=%SERVER-AGII_APDEVTW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APDEVTW01%\D$\Inetpub\Int\CR.Maintenance.INT.TR_filling.Maintenance
		set POOLAPPNAME=CR.App.INT.Maintenance-TR_Filing
	)
	if "%ENVIRONMENT%"=="SIT" (
		set SERVERNAME=%SERVER-AGII_APSITW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APSITW01%\D$\Inetpub\WebSite\CR.Maintenance.SIT
		set POOLAPPNAME=CR.App.SIT
	)
	if "%ENVIRONMENT%"=="TRN" (
		set SERVERNAME=%SERVER-AGII_APTRNQW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APTRNQW01%\D$\inetpub\WebSite\CR.Maintenance.TR
		set POOLAPPNAME=CR.App.UAT
	)	
	if "%ENVIRONMENT%"=="PERF01" (
		set SERVERNAME=%SERVER-AGII_APPRFTW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APPRFTW01%\D$\inetpub\WebSite\CR.Maintenance.TR
		set POOLAPPNAME=CR.App.PERF
	)	
	if "%ENVIRONMENT%"=="PERF02" (
		set SERVERNAME=%SERVER-AGII_APPRFTW02%
		set DESTINATIONPATH=\\%SERVER-AGII_APPRFTW02%\D$\inetpub\WebSite\CR.Maintenance.TR
		set POOLAPPNAME=CR.App.PERF
	)	
)
if "%APPNAME%"=="WebService" (
	if "%ENVIRONMENT%"=="DEV" (
		set SERVERNAME=%SERVER-AGII_APDEVTW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APDEVTW01%\D$\Inetpub\CR.WS.Int.Maintenance
		set POOLAPPNAME=CR.WS.Int.Maint
	)
	if "%ENVIRONMENT%"=="UAT" (
		set SERVERNAME=%SERVER-AGII_APDEVTW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APDEVTW01%\D$\Inetpub\CR.WS.UAT.Candidate
		set POOLAPPNAME=CR.WS.UAT.Candidate
	)
	if "%ENVIRONMENT%"=="CRPRJ" (
		set SERVERNAME=%SERVER-AGII_APDEVTW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APDEVTW01%\D$\Inetpub\CR.WS.PRJ
		set POOLAPPNAME=CR.WS.PRJ
	)
	if "%ENVIRONMENT%"=="TR_PR" (
		set SERVERNAME=%SERVER-AGII_APSITW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APSITW01%\D$\Inetpub\CR.WS.Int.Maintenance-TR_Filing
		set POOLAPPNAME=TRFiling
	)
	if "%ENVIRONMENT%"=="SIT" (
		set SERVERNAME=%SERVER-AGII_APSITW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APSITW01%\D$\Inetpub\Argonaut.ES.RQBI.WebService
		set POOLAPPNAME=ColonyRater
	)
	if "%ENVIRONMENT%"=="TRN" (
		set SERVERNAME=%SERVER-AGII_APTRNQW01%
		set DESTINATIONPATH=\\%SERVER-AGII_APTRNQW01%\D$\inetpub\Argonaut.ES.RQBI.WebService
		set POOLAPPNAME=ColonyRater
	)	
	if "%ENVIRONMENT%"=="PERF01" (
		set SERVERNAME=%SERVER-AGII-R3APPRFW01%
		set DESTINATIONPATH=\\%SERVER-AGII-R3APPRFW01%\D$\inetpub\???
		set POOLAPPNAME=???
	)	
	if "%ENVIRONMENT%"=="PERF02" (
		set SERVERNAME=%SERVER-AGII-R3APPRFW02%
		set DESTINATIONPATH=\\%SERVER-AGII-R3APPRFW02%\D$\inetpub\???
		set POOLAPPNAME=???
	)	
)
echo.
echo.
echo Gathered Information
echo Gathered Information >> %LOGFILE%
echo.
echo. >> %LOGFILE%
echo Server Name: %SERVERNAME%
echo Source Path: %SOURCEPATH%
echo Destination Path: %DESTINATIONPATH%
echo Pool Application Name: %POOLAPPNAME%

echo Server Name: %SERVERNAME% >> %LOGFILE%
echo Source Path: %SOURCEPATH% >> %LOGFILE%
echo Destination Path: %DESTINATIONPATH% >> %LOGFILE%
echo Pool Application Name: %POOLAPPNAME% >> %LOGFILE%
echo.
echo. >> %LOGFILE%
:: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
:: @ Backing up destination path @
:: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo.
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo Backing up destination path - %APPNAME%
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
set BACKUPBASEPATH=\\%SERVERNAME%\D$\ColonyRater\Backup\%ENVIRONMENT%\%APPNAME%\
set BACKUPFILENAME=%_isodate%-%APPNAME%-%ENVIRONMENT%.zip
set BACKUPPATH=%BACKUPBASEPATH%%BACKUPFILENAME%
echo.

echo Backup Base Path: %BACKUPBASEPATH%
echo Backup File Name: %BACKUPFILENAME%
echo Backup Path: %BACKUPPATH%
echo.

:: Write to log file
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ >> %LOGFILE%
echo Backing up destination path - %APPNAME% >> %LOGFILE%
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ >> %LOGFILE%
echo.  >> %LOGFILE%

echo Backup Base Path: %BACKUPBASEPATH% >> %LOGFILE%
echo Backup File Name: %BACKUPFILENAME% >> %LOGFILE%
echo Backup Path: %BACKUPPATH% >> %LOGFILE%
echo. >> %LOGFILE%

echo Compressing for backup using 7z is in progress...
echo Compressing for backup using 7z is in progress... >> %LOGFILE%
echo @@@ "C:\Program Files\7-Zip\"7z a %BACKUPPATH% %DESTINATIONPATH%
echo @@@ "C:\Program Files\7-Zip\"7z a %BACKUPPATH% %DESTINATIONPATH% >> %LOGFILE%

echo Using Net Use to remote connect
echo Using Net Use to remote connect >> %LOGFILE%
net use \\%SERVERNAME%\D$ %UPWD% /user:argous\%UNAME%

"C:\Program Files\7-Zip\"7z a %BACKUPPATH% %DESTINATIONPATH%
:: timeout /t 30 /nobreak > NUL
:: timeout /t 3 > NUL
echo.
echo. >> %LOGFILE%
echo Compressed for backup
echo Compressed for backup >> %LOGFILE%
dir %BACKUPPATH%
dir %BACKUPPATH% >> %LOGFILE%
echo.
echo. >> %LOGFILE%

:: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
:: @ Copy files from source to destination @
:: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo.
echo. >> %LOGFILE%
echo Copying files from source to destination...
echo Source Path: %SOURCEPATH%
echo Destination Path: %DESTINATIONPATH%
echo.

echo Copying files from source to destination... >> %LOGFILE%
echo Source Path: %SOURCEPATH% >> %LOGFILE%
echo Destination Path: %DESTINATIONPATH% >> %LOGFILE%
echo. >> %LOGFILE%

echo Setting up file exclude for copying
echo Setting up file exclude for copying >> %LOGFILE%
echo \Config > excludedfileslist.txt
echo Web.config >> excludedfileslist.txt

echo @@@ xcopy %SOURCEPATH%* %DESTINATIONPATH%\ /E /C /I /Q /G /H /R /K /Y /Z /J /exclude:excludedfileslist.txt
echo @@@ xcopy %SOURCEPATH%* %DESTINATIONPATH%\ /E /C /I /Q /G /H /R /K /Y /Z /J /exclude:excludedfileslist.txt >> %LOGFILE%
xcopy %SOURCEPATH%* %DESTINATIONPATH%\ /E /C /I /Q /G /H /R /K /Y /Z /J /exclude:excludedfileslist.txt
:: timeout /t 3 > NUL
echo Copied files from source to destination.
echo Copied files from source to destination. >> %LOGFILE%
echo.
echo. >> %LOGFILE% 
dir %DESTINATIONPATH%
dir %DESTINATIONPATH% >> %LOGFILE%

echo.
echo.
echo.  >> %LOGFILE%
echo.  >> %LOGFILE%
echo End of Process
echo End of Process  >> %LOGFILE%
echo.
echo.  >> %LOGFILE%
goto :eof

:argnotfound
echo.
echo Arguments are missing in the user input.
echo Usage: %0 ^<Environment^> ^<Application Name^> ^<User Name^> ^<User Password^>
echo Note: For Application Name, please pass on either WebApp/Rater/WebService
echo Example: %0 DEV WebApp Sathya.Bandla pwd

echo. >> %LOGFILE%
echo Arguments are missing in the user input. >> %LOGFILE%
echo Usage: %0 ^<Environment^> ^<Application Name^> ^<User Name^> ^<User Password^> >> %LOGFILE%
echo Note: For Application Name, please pass on either WebApp/Rater/WebService >> %LOGFILE%
echo Example: %0 DEV WebApp Sathya.Bandla pwd >> %LOGFILE%
exit /B 1